/**
 * \file
 * This is a doxygen comment belonging to the whole file. Doxygen comments can
 * be started by double asterisks.
 */


/**
 * \mainpage
 * This is the documentation for the doxygen main page.
 *
 */


#include "foo.h"
#include <assert.h>
#include <stdio.h>


/**
 * This is the doxygen documentation for the C main function.
 */
int main(int argc, char* argv[])
{
    assert(argv != NULL);

    printf("hello minimal template\n");

    print_args(argc, argv);

    return 0;
}

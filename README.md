# C Project Template

A minimal and an advanced template for makefile based C projects.

The minimal example just builds an executable and that's it.

The advanced template contains:

* Out of source tree builds
* Source code formatting with [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html)
* Linting with [ClangTidy](https://clang.llvm.org/extra/clang-tidy/)
* Static analysis with [CppCheck](http://cppcheck.sourceforge.net/)
* Model checking with [CBMC](http://www.cprover.org/cbmc/)
* Unit tests with [greatest](https://github.com/silentbicycle/greatest)
* Index generation with [Ctags](http://ctags.sourceforge.net/)
* Developer documentation with [doxygen](http://www.doxygen.org/)
* The makefile itself (for [GNU make](https://www.gnu.org/software/make/manual/make.html))

This README.md file is [Markdown](https://de.wikipedia.org/wiki/Markdown) formatted. It is also used as main page for the generated doxygen documentation.

To adapt the template for a project:

1. Change LICENSE file as appropriate
2. Change PROJECT_NAME in doxyfile
3. Put source code files into ./src/
4. Add source code files to makefile, modify it as needed

The following make targets are available.

Build binary:

    make

Check source code:

    make check

Generate documentation:

    make doc

Generate TAGS file:

    make tags

Build and run unit tests:

    make test

Delete artifacts created by make:

    make clean

and 

    make testclean

/**
 * \file
 * This is a doxygen comment belonging to the whole file. Doxygen comments can
 * be started by double asterisks.
 */


#ifndef FOO_H
#define FOO_H


/**
 * This is the doxygen documentation for the print_args function.
 */
int print_args(int argc, char* argv[]);


#endif

#include "foo.h"
#include "greatest.h"


TEST test_print_args(void)
{
    int r = print_args(1, NULL);

    ASSERT_EQ(-1, r);

    PASS();
}


SUITE(test_suite)
{
    RUN_TEST(test_print_args);
}


GREATEST_MAIN_DEFS();


int main(int argc, char* argv[])
{
    GREATEST_MAIN_BEGIN();

    RUN_SUITE(test_suite);

    GREATEST_MAIN_END();
}

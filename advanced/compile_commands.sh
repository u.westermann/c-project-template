#! /usr/bin/env dash

set -eu

# create compilation database used by clang-tidy
make clean
bear make
